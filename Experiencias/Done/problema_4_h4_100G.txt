Runs: 50

Population size: 100

Max generations: 100

//--------------------------------

Selection: tournament, roulette wheel

Tournament size: 2

//--------------------------------

Recombination: PMX, CX, OX1

Recombination probability: 0.7

//--------------------------------

Mutation: binary

Mutation probability: 0.001

//--------------------------------

Fitness type: 0

//--------------------------------

Problem file: ./DataSets/DataSetCutOptimization4_Pent_h4.txt

//--------------------------------

Statistic: BestIndividual
Statistic: BestAverage
