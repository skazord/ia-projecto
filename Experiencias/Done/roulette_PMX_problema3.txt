Runs: 50

Population size: 100

Max generations: 100

//--------------------------------

Selection: roulette wheel, tournament

Tournament size: 2

//--------------------------------

Recombination: PMX

Recombination probability: 0.7

//--------------------------------

Mutation: binary

Mutation probability: 0.001

//--------------------------------

Probability of 1s: 0.05

Fitness type: 0

//--------------------------------

Problem file: ./DataSets/DataSetCutOptimization3.txt

//--------------------------------

Statistic: BestIndividual
Statistic: BestAverage