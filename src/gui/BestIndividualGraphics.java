/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import javax.swing.border.MatteBorder;

/**
 *
 * @author Gabriel Rodrigues
 */
public class BestIndividualGraphics extends JDialog{
    private JPanel jp;
    private ArrayList<Color> colors;
    private int width;
    private int height;
    private int[][] roll;
    private int numPieces;
    private double fitness;
    

    public BestIndividualGraphics(double fitness, int numPieces, int[][] roll){
        this.height = roll.length;
        this.width = roll[0].length;
        this.numPieces = numPieces;
        this.roll = roll;
        this.fitness = fitness;
        this.jp = new JPanel();
        this.colors = new ArrayList<Color>();
        
        initiateFrame();
        showIndividual();
    }

    private void setColors(int numPieces) {
//        float saturation = 0.6f;
//        float luminance = 0.9f;   
//        double margin = 1/numPieces;
//        float acumulate = 0.01f;
//        
//        for (int i = 0; i < numPieces; i++) {
//            colors.add(Color.getHSBColor(acumulate, saturation, 0.9f));
//            acumulate += acumulate;
//        }
        
        colors.add(Color.GRAY);
        //colors.add(Color.ORANGE);
        colors.add(Color.PINK);
        colors.add(new Color(116, 223, 252)); // sky blue
        colors.add(new Color(138, 255, 202)); // mint green
        colors.add(new Color(255, 211, 181)); // light magma
        colors.add(new Color(205, 255, 23)); // lemon
        colors.add(new Color(171, 159, 245)); // light strong blue
        colors.add(new Color(224, 212, 72)); // gold
        colors.add(new Color(212, 106, 106)); // soft red
        colors.add(new Color(255, 225, 0)); // pikachu
        colors.add(new Color(164, 204, 137)); // soldier green
        colors.add(new Color(255, 229, 168)); // light orange
        colors.add(new Color(255, 105, 5)); // magma
        colors.add(new Color(71, 173, 183)); // dark cyan
        colors.add(new Color(143, 26, 115)); // dark pink
        colors.add(new Color(128, 21, 21)); // dark red
        colors.add(new Color(189, 174, 15)); // copper
        colors.add(new Color(212, 153, 15)); // dark orange
        colors.add(new Color(171, 72, 7)); // terracotta
    }
    
    private void initiateFrame(){
        this.setBackground(Color.white);
        this.setSize(width*50, height*50);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setName("Bets individual - Fitness: " + fitness);
        this.setTitle("Bets individual - Fitness: " + fitness);
        this.setModal(true);
        this.setVisible(false);
        this.setModalExclusionType(Dialog.ModalExclusionType.TOOLKIT_EXCLUDE);
        jp.setLayout(new GridLayout(height, width));
        jp.setBorder(new BevelBorder(BevelBorder.RAISED));
        add(jp);
        
        setColors(numPieces);
    }
    
    public void showIndividual(){
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                //JLabel jl = new JLabel("  ");
                JButton jb = new JButton("  ");
                jb.setContentAreaFilled(false);
                jb.setOpaque(true);
                jb.setBorder(new MatteBorder(1, 1, 1, 1, Color.DARK_GRAY));
                if (roll[i][j] == 0) {
                    jb.setBackground(Color.white);
                }
                else{
                    jb.setBackground(colors.get(roll[i][j] -1));
                }
                
                jb.setText(roll[i][j]+"");
                
                jp.add(jb);
            }
        }
    }
    
    
}
