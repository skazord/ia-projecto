/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cutOptimization;

import ga.Problem;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Scanner;

import knapsack.Item;
import knapsack.KnapsackIndividual;

/**
 *
 * @author Gabriel Rodrigues
 */
public class CutOptimization implements Problem <CutOptimizationIndividual>{
    

    public static final int SIMPLE_FITNESS = 0;
    public static final int PENALTY_FITNESS = 1;    
    private Piece[] pieces;
    private double maximumWeight;//nao
    private double prob1s;
    private int fitnessType = SIMPLE_FITNESS;
    private int rollHeight;
    private double maxVP;//nao

    public CutOptimization(Piece[] pieces, int rollHeight, double prob1s) {
        if (pieces == null) {
            throw new IllegalArgumentException();
        }
        this.pieces = new Piece[pieces.length];
        System.arraycopy(pieces, 0, this.pieces, 0, pieces.length);
        this.rollHeight = rollHeight;
        this.prob1s = prob1s;
        //maxVP = computeMaxVP();
    }
    
    public CutOptimizationIndividual getNewIndividual(){
        return new CutOptimizationIndividual(this, prob1s);
    }

    public int getNumItems() {
        return pieces.length;
    }   

    public Piece getPiece(int index) {
        return (index >= 0 && index < pieces.length) ? pieces[index] : null;
    }

    public double getMaximumWeight() {
        return maximumWeight;
    }
    
    public double getProb1s(){
        return prob1s;
    }
    
    public void setProb1s(double prob1s){
        this.prob1s = prob1s;
    }
    
    public int getFitnessType(){
        return fitnessType;
    }
    
    public void setFitnessType(int fitnessType){
        this.fitnessType = fitnessType;
    }

    public double getMaxVP() {
        return maxVP;
    }

    @Override
    public String toString() {
        /*
        * File Structure:
        * Numero de pe�as
        * Altura do roll
        * Tamanho da grid: XxY
        * Posiç�o inicial: x,y
        * Grid:
        * 0 1 0 0
        * 0 1 1 1
        * 0 1 0 0
        */
        StringBuilder sb = new StringBuilder();
        sb.append("# de peças: ");
        sb.append(pieces.length);
        sb.append("\n");        
        sb.append("Altura do roll: ");
        sb.append(rollHeight);
        sb.append("\n");
        sb.append("Peças:");
        sb.append("\nID\tDimensions\tAngle");
        for (Piece piece : pieces) {
            sb.append(piece);
        }
        return sb.toString();
    }

    public int getRollHeight() {
        return rollHeight;
    }

    private double computeMaxVP() {
//        double max = pieces[0].value / pieces[0].weight, divVP;
//        for (int i = 1; i < pieces.length; i++) {
//            divVP = pieces[i].value / pieces[i].weight;
//            if (divVP > max) {
//                max = divVP;
//            }
//        }
        //return max;
        return 0;
    }

    public static CutOptimization buildKnapsack(String path) throws IOException {
        CutOptimization dataSet = fileParse(path);

        int n = dataSet.getNumItems();
        int pM = dataSet.getRollHeight();

        Piece[] pieces = new Piece[n];
        pieces = dataSet.pieces;

        for (int i = 0; i < pieces.length; i++) {
            pieces[i] = new Piece((i + 1), pieces[i].getxBegin(), pieces[i].getyBegin(), pieces[i].getMatrixNorth());
        }

        return new CutOptimization(pieces, pM, 0.5);
    }
//    
//    public static CutOptimization buildCutOptimization(String path) throws FileNotFoundException {
//        Hashtable hash = fileParse(path);
//        return new CutOptimization(pieces, SIMPLE_FITNESS, SIMPLE_FITNESS);
//    }

    public static CutOptimization fileParse(String path) throws FileNotFoundException {
        /*
        * File Structure:
        * Numero de pe�as
        * Altura do roll
        * Tamanho da grid: XxY
        * Posi��o inicial: x,y
        * Grid:
        * 0 1 0 0
        * 0 1 1 1
        * 0 1 0 0
        */

        File file = new File(path);
        Scanner input = new Scanner(file);

        int numPecas = Integer.parseInt(input.next());
        int rollHeight =  Integer.parseInt(input.next());

        Piece pieces[] = new Piece[numPecas];

        for(int i=0;i<numPecas;i++) {
            String pieceGridSize = input.next();
            int pieceLenght = Character.getNumericValue(pieceGridSize.charAt(0));
            int pieceHeight = Character.getNumericValue(pieceGridSize.charAt(2));
            int[][] pieceMatrix = new int[pieceHeight][pieceLenght];
            String piecePosition = input.next();
            int pieceY = Character.getNumericValue(piecePosition.charAt(0));
            int pieceX = Character.getNumericValue(piecePosition.charAt(2));

            for(int o=0;o<pieceHeight;o++){
                for(int p=0;p<pieceLenght;p++){
                    String next = input.next();
                    if(next.equals("1")){
                        pieceMatrix[o][p] = (i+1);
                    }
                }
            }

            pieces[i] = new Piece(i+1, pieceX, pieceY, pieceMatrix);
        }

        input.close();

        return new CutOptimization(pieces, rollHeight, 0.5);
    }
}

