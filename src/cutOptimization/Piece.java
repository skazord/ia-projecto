/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cutOptimization;

/**
 *
 * @author Gabriel Rodrigues
 */
public class Piece {
    private final int id;
    private int xBegin;
    private int yBegin;// sao necessarios?
    private int size; //numero de blocos que ocupa a peça
    private final int[][] matrixNorth;
    private int[][] matrixEast; //90 graus
    private int[][] matrixSouth; //180 graus
    private int[][] matrixWest; //270 graus
    private TypeAngle currentAngle;

    public Piece(int id, int xBegin, int yBegin, int[][] matrix) {
        this.id = id;
        this.xBegin = xBegin;
        this.yBegin = yBegin;
        this.matrixNorth = matrix;
        this.matrixEast = rotateMatrix(matrix, TypeAngle.ANGLE90);
        this.matrixSouth = rotateMatrix(matrix, TypeAngle.ANGLE180); //wrong is doing something wrong lol
        this.matrixWest = rotateMatrix(matrix, TypeAngle.ANGLE270);
        this.currentAngle = TypeAngle.DEFAULT;
        calculateSize();
    }

    public int getId() {
        return id;
    }

    public int getxBegin() {
        return xBegin;
    }

    public int getyBegin() {
        return yBegin;
    }

    public int[][] getMatrix(int numaAngle) {
        
        TypeAngle angle = TypeAngle.getAngle(numaAngle);
        switch(angle){
            case ANGLE90:
                return matrixEast;
            case ANGLE180:
                return matrixSouth;
            case ANGLE270:
                return matrixWest;
            default:
                return matrixNorth;
        }
    }

    private void calculateSize() {
        size = 0;
        for (int i = 0; i < matrixNorth.length; i++) { //matrix.length altura
            for (int j = 0; j < matrixNorth[0].length; j++) { //matrix[0].length largura
                size += matrixNorth[i][j]/id;
            }
        }
        //System.out.println("Size " + size); //Funciona!
    }
    
    public String toString() {
        String pieceDesc =  "\n" + id + "\t" + matrixNorth[0].length + "x" + matrixNorth.length + "\t\t" + currentAngle;
        String matrixDesc = "\n\n\t";
        int extraLines = String.valueOf(id).length() > 1? matrixNorth[0].length : 0;
        for (int i = 0; i < (matrixNorth[0].length)*3 + 2 + extraLines; i++) {
            matrixDesc += "-";
        }
        matrixDesc += "\n\t";
        for (int i = 0; i < matrixNorth.length; i++) {
            matrixDesc += "|";
            for (int j = 0; j < matrixNorth[0].length; j++) {
                matrixDesc += String.valueOf(id).length() > 1 && matrixNorth[i][j] == 0? "  " + "00" : "  " + matrixNorth[i][j];
            }
            matrixDesc += "|";
            matrixDesc += "\n\t";
        }
        for (int i = 0; i < (matrixNorth[0].length)*3 + 2 + extraLines; i++) {
            matrixDesc +="-";
        }
        return pieceDesc.concat(matrixDesc);
    }

    private int[][] rotateMatrix(int[][] matrix, TypeAngle typeAngle) {
        
        switch(typeAngle){
            case ANGLE90:
                return rotateNorthToEast(matrix);
            case ANGLE180:
                return rotateNorthToSouth(matrix);
            case ANGLE270:
                return rotateNorthToWest(matrix);
            default:
                return matrixNorth;
        }
    }

    private int[][] rotateNorthToEast(int[][] matrix) {
        int[][] east = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                east[j][(matrix.length) - i - 1] = matrix[i][j];
            }
        }
        
//        for (int i = 0; i < east.length; i++) {
//            for (int j = 0; j < east[0].length; j++) {
//                
//                System.out.print(east[i][j] + " ");
//            }
//            System.out.print("\n");
//        }
        return east;
    }
    
    private int[][] rotateNorthToWest(int[][] matrix) {
        int[][] west = new int[matrix[0].length][matrix.length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                west[(matrix[0].length) - j - 1][i] = matrix[i][j];
            }
        }
        
//        for (int i = 0; i < west.length; i++) {
//            for (int j = 0; j < west[0].length; j++) {
//                
//                System.out.print(west[i][j] + " ");
//            }
//            System.out.print("\n");
//        }
        
        return west;
    }
    
    private int[][] rotateNorthToSouth(int[][] matrix) {
        int[][] south = new int[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                south[matrix.length - i - 1][matrix[0].length - j - 1] = matrix[i][j];
            }
        }
//        for (int i = 0; i < south.length; i++) {
//            for (int j = 0; j < south[0].length; j++) {
//                
//                System.out.print(south[i][j] + " ");
//            }
//            System.out.print("\n");
//        }
        return south;
    }
    
    public int getSize() {
        return size;
    }

    public int[][] getMatrixNorth() {
        return matrixNorth;
    }

    public int[][] getMatrixEast() {
        return matrixEast;
    }

    public int[][] getMatrixSouth() {
        return matrixSouth;
    }

    public int[][] getMatrixWest() {
        return matrixWest;
    }
}
