/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cutOptimization;

/**
 *
 * @author Gabriel Rodrigues
 */
public enum TypeAngle {

    DEFAULT (0),
    ANGLE90 (1),
    ANGLE180 (2),
    ANGLE270 (3); //270º == -90º
    
    private final int angle;

    private TypeAngle(int angle) {
        this.angle = angle;
    }

    public static TypeAngle getAngle(int i) {
        switch(i){
            case 1:
                return ANGLE90;
            case 2:
                return ANGLE180;
            case 3:
                return ANGLE270;
            default:
                return DEFAULT;
        }
    }

    @Override
    public String toString() {
        switch(getAngle(angle)){
            case ANGLE90:
                return "ANGLE90";
            case ANGLE180:
                return "ANGLE180";
            case ANGLE270:
                return "ANGLE270";
            default:
                return "DEFAULT";
        }
    }
    
    
}
