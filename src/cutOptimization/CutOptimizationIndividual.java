package cutOptimization;

import com.sun.jmx.remote.util.OrderClassLoaders;
import static ga.BitVectorIndividual.ONE;
import ga.OrderPairVectorIndividual;
import java.util.ArrayList;
import knapsack.Knapsack;

/**
 *
 * @author Gabriel Rodrigues
 */
public class CutOptimizationIndividual extends OrderPairVectorIndividual <CutOptimization>{
    
    private int wastedSpace;
    private int numberCuts;
    private int rollLenght;
    private int[][] roll;
    private int usedSpace;
    private ArrayList<Integer> piecesIDOut;
    
    public CutOptimizationIndividual(CutOptimization problem, double prob1s){
        super(problem, problem.getNumItems(), prob1s);
        rollLenght = calculateMaximumLength(problem);
        //rollLenght = 3;
        piecesIDOut = new ArrayList();
        deterministicFillRoll();
        //System.out.println(this.toString());
        //System.out.println("STOP");
    }
    
    public CutOptimizationIndividual(CutOptimizationIndividual original) {
        super(original);
        this.wastedSpace = original.wastedSpace;
        this.numberCuts = original.numberCuts;
        this.rollLenght = original.rollLenght;
        this.piecesIDOut = original.piecesIDOut;
        this.usedSpace = original.usedSpace;
        this.roll = original.getRoll();
    }

    public int[][] getRoll() {
        return roll;
    }
    
    private void deterministicFillRoll(){
        roll = new int[problem.getRollHeight()][rollLenght];
        piecesIDOut.clear();
        for (int i = 0; i < problem.getNumItems(); i++) {
            switch(TypeAngle.getAngle(genome[i][1])){
                case ANGLE90:
                    putPiece(problem.getPiece(genome[i][0] - 1).getMatrixEast(), genome[i][0]);
                    break;
                case ANGLE180:
                    putPiece(problem.getPiece(genome[i][0] - 1).getMatrixSouth(), genome[i][0]);
                    break;
                case ANGLE270:
                    putPiece(problem.getPiece(genome[i][0] - 1).getMatrixWest(), genome[i][0]);
                    break;
                default:
                    putPiece(problem.getPiece(genome[i][0] - 1).getMatrixNorth(), genome[i][0]);
            }
        }
    }
    
    public double computeFitness() {
        deterministicFillRoll();
        trimRoll();
        numberCuts =  countNumberCuts();
        usedSpace = countUsedSpace();
        
        wastedSpace = rollLenght*problem.getRollHeight() - usedSpace;
        
        fitness = wastedSpace + numberCuts;
        
        fitness -= fitness/(wastedSpace + numberCuts);
        
        switch(problem.getFitnessType()){
            case CutOptimization.SIMPLE_FITNESS:
                fitness = (!piecesIDOut.isEmpty())? problem.getRollHeight()*calculateMaximumLength(problem) : fitness;
                break;
            case CutOptimization.PENALTY_FITNESS:
                fitness = (!piecesIDOut.isEmpty())? getPenalty(fitness) : fitness;
                break;
        }
        
        //enquanto maior a fitness pior é o individuo
        //System.out.println(this.toString());
        return fitness;
    }    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nWasted Space: " + wastedSpace + " (Roll Height: " + problem.getRollHeight()+ ")");
        sb.append("\nNumber of missing pieces: " + piecesIDOut.size());
        sb.append("\nUsed space: " + usedSpace);
        sb.append("\nNumber of cuts: " + numberCuts);        
        sb.append("\nfitness: " + fitness);     
        sb.append("\nRoll Lenght: " + rollLenght);
        sb.append("\nRoll: ");
        
        String rollString = "\n";
        
        for (int i = 0; i < roll.length; i++) {
            for (int j = 0; j < roll[0].length; j++) {
                if (String.valueOf(roll[i][j]).length() == 1) {
                    rollString += "0";
                }
                rollString += roll[i][j] + "  ";
            }
            rollString += "\n";
        }
        rollString += "\n";
        
        for (int i = 0; i < problem.getNumItems(); i++) {
            rollString += "Piece: " + genome[i][0] + " - " + "Angle: " + TypeAngle.getAngle(genome[i][1]) + "\n";
        }
        
        sb.append(rollString);
        return sb.toString();
    }
    
    @Override
    public Object clone() {
        return new CutOptimizationIndividual(this);
    }    

    private double getPenalty(double fit) {
        fit = fit == 0? piecesIDOut.size() * 100 : fit*(piecesIDOut.size() + 1);
        //a peça que não entra no roll não vai ser colocada, todo individuo que não tenha todas as peças será gravemente penalizado.
        return fit;
    }

    private int calculateMinimumLength(CutOptimization problem) {
        double usedSpace = 0;
        for (int i = 0; i < genome.length; i++) {
            usedSpace += problem.getPiece(genome[i][0] - 1).getSize();
        }
        double ceil = Math.ceil(usedSpace / problem.getRollHeight());
        return (int) ceil;
    }

    private void putPiece(int[][] piece, int pieceID) {
        if (piece.length > problem.getRollHeight()) {
            piecesIDOut.add(pieceID);
            //System.out.println("NO PUSO LA PIEZA PORQUE ES MUY ALTA");
            return;
        }
        for (int j = 0; j < rollLenght; j++) {
            for (int i = problem.getRollHeight() - 1; i >= 0; i--) {
                if(searchPieceFitsPosition(i,j, piece)){
                    return;
                }
            }
        }
        
        //System.out.println(this.toString()); //more bugs everywhere!!
        piecesIDOut.add(pieceID);
        System.out.println("Nao pus a peca");
    }

    private boolean searchPieceFitsPosition(int i, int j, int[][] piece) {
        for (int y = 0; y < piece.length; y++) {
            for (int x = 0; x < piece[0].length; x++) {
                if (j + x >= rollLenght - 1) {
                        rollLenght = j + x + 2;
                        int[][] aux = roll;
                        roll = new int[problem.getRollHeight()][rollLenght];
                        roll = fillNewSizedRoll(roll, aux, aux.length, aux[0].length);
                }
                //System.out.println(i+"+"+y + " - " + j+"+"+x);
                if (piece[y][x] != 0) {
                    if (i + y >= problem.getRollHeight() || roll[i + y][j + x] != 0) {
                        return false;
                    }
                }
            }
        }
        
        //real put piece
        //System.out.println("PUSO LA PIEZA");
        for (int y = 0; y < piece.length; y++) {
            for (int x = 0; x < piece[0].length; x++) {
                roll[i + y][j + x] += piece[y][x];
            }
        }
        
        return true;
    }

    private int[][] fillNewSizedRoll(int[][] roll, int[][] aux, int height, int width) {
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                roll[i][j] = aux[i][j];
            }
        }
        return roll;
    }
    
    @Override
    public int[][] getGenome(){
        return super.getGenome();
    }

    private void trimRoll() {
        
        for (int j = roll[0].length - 1; j > 0 ; j--) {
            for (int i = 0; i < roll.length; i++) {
                if (roll[i][j] != 0) {
                    if (j != roll[0].length - 1) {
                        rollLenght = j + 1;
                        int[][] aux = roll;
                        roll = new int[problem.getRollHeight()][rollLenght];
                        roll = fillNewSizedRoll(roll, aux, roll.length, roll[0].length);
                        return;
                    }
                    else{
                        return;
                    }
                }
            }
        }
    }

    private int calculateMaximumLength(CutOptimization problem) {
        int maxLenght = 0;
        for (int i = 0; i < genome.length; i++) {
            maxLenght += problem.getPiece(genome[i][0] - 1).getMatrixNorth().length < problem.getPiece(genome[i][0] - 1).getMatrixNorth()[0].length? problem.getPiece(genome[i][0] - 1).getMatrixNorth()[0].length : problem.getPiece(genome[i][0] - 1).getMatrixNorth().length;
        }
        return maxLenght;
    }
    
    private int countNumberCuts(){
        int hCuts = 0, vCuts = 0;
        for (int i = 0; i < problem.getRollHeight(); i++) {
            for (int j = 0; j < rollLenght; j++) {
                if (i + 1 < problem.getRollHeight() && roll[i][j] != roll[i+1][j]) {
                    hCuts++;
                }
                if (j + 1 < rollLenght && roll[i][j] != roll[i][j+1]) {
                    vCuts++;
                }
            }
        }
        return hCuts+vCuts;
    }

    private int countUsedSpace() {
        int space = 0;
        for (int i = 0; i < genome.length; i++) {
            space += problem.getPiece(genome[i][0] - 1).getSize();
        }
        for (int i = 0; i < piecesIDOut.size(); i++) {
            space -= problem.getPiece(piecesIDOut.get(i) - 1).getSize();
        }
        
        return space;
    }
}

