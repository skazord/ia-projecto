/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga.geneticOperators;

import ga.GeneticAlgorithm;
import ga.Individual;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Rodrigues
 */
public class RecombinationOX1 <I extends Individual> extends Recombination<I>{
    public RecombinationOX1(double probability) {
        super(probability);
    }
    //RecombinationOneCut

    @Override
    public void run(I ind1, I ind2) {
        int n = ind1.getNumGenes();

        if (n != ind2.getNumGenes()) {
            try {
                throw new Exception("permutations not same size");
            } catch (Exception ex) {
                Logger.getLogger(RecombinationPMX.class.getName()).log(Level.SEVERE, null, ex);
            }
            return;
        }

        // select cutting points
        int cuttingPoint1 = GeneticAlgorithm.random.nextInt(n);
        int cuttingPoint2 = GeneticAlgorithm.random.nextInt(n - 1);

        if (cuttingPoint1 == cuttingPoint2) {
                cuttingPoint2 = n - 1;
        } else if (cuttingPoint1 > cuttingPoint2) {
                int swap = cuttingPoint1;
                cuttingPoint1 = cuttingPoint2;
                cuttingPoint2 = swap;
        }
        
        Individual ind2PAux = (I) ind2.clone();
        Individual ind1PAux = (I) ind1.clone();
        HashMap<Integer,Integer> onCuts1 = new HashMap<Integer, Integer>();
        HashMap<Integer,Integer> onCuts2 = new HashMap<Integer, Integer>();

        // exchange between the cutting points
        for (int i = cuttingPoint1; i <= cuttingPoint2; i++) {
            ind1.swapGenes(ind2, i);
            onCuts1.put(ind1.getGene(i)[0], ind1.getGene(i)[0]);
            onCuts2.put(ind2.getGene(i)[0], ind2.getGene(i)[0]);
        }

        int counter2 = cuttingPoint2 + 1 == n? 0 : cuttingPoint2 + 1;
        int counter1 = cuttingPoint2 + 1 == n? 0 : cuttingPoint2 + 1;

        // fill in remaining slots with replacements
        for (int i = cuttingPoint2 + 1 == n? 0 : cuttingPoint2 + 1; i != cuttingPoint2; i = i+1 == n? 0 : i+1) {
            int[] geneParent1 = ind1PAux.getGene(i);
            int[] geneParent2 = ind2PAux.getGene(i);

            if (!onCuts2.containsKey(geneParent2[0])) {
                ind2.setGene(counter2, geneParent2[0], geneParent2[1]);
                counter2 = counter2 + 1 == n? 0 : counter2 + 1;
            }
            if (!onCuts1.containsKey(geneParent1[0])) {
                ind1.setGene(counter1, geneParent1[0], geneParent1[1]);
                counter1 = counter1 + 1 == n? 0 : counter1 + 1;
            }
        }

        int[] geneParent1 = ind1PAux.getGene(cuttingPoint2);
        int[] geneParent2 = ind2PAux.getGene(cuttingPoint2);
        
        if (!onCuts2.containsKey(geneParent2[0])) {
            ind2.setGene(counter2, geneParent2[0], geneParent2[1]);
            counter2 = counter2 + 1 == n? 0 : counter2 + 1;
        }
        if (!onCuts1.containsKey(geneParent1[0])) {
            ind1.setGene(counter1, geneParent1[0], geneParent1[1]);
            counter1 = counter1 + 1 == n? 0 : counter1 + 1;
        }
    }
    
    @Override
    public String toString(){
        return "OX1 recombination (" + probability + ")";
    }  
}
