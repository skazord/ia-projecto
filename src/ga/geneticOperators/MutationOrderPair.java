package ga.geneticOperators;

import ga.BitVectorIndividual;
import ga.GeneticAlgorithm;
import ga.OrderPairVectorIndividual;

public class MutationOrderPair <I extends OrderPairVectorIndividual> extends Mutation<I> {

    public MutationOrderPair(double probability) {
        super(probability);
    }


    public void run(I ind) {
        for(int i = 0; i < ind.getNumGenes(); i++){
            double value = GeneticAlgorithm.random.nextDouble();
            if(value < probability){
                double val;
                if ((val = GeneticAlgorithm.random.nextDouble()) <= 0.5) {
                    mutatePairVectorPiece(i, ind);
                }
                else{
                    mutatePairVectorAngle(i, ind);
                }
            }
        }
    }
    
    @Override
    public String toString(){
        return "Order pair mutation (" + probability + ")";
    }

    private void mutatePairVectorPiece(int i, I ind) {
        int changePosition = -1;
        while(changePosition == -1 || changePosition == i){
            changePosition = GeneticAlgorithm.random.nextInt(ind.getNumGenes());
        }
        int[] auxGene = ind.getGene(i);
        ind.setGene(i, ind.getGene(changePosition)[0], ind.getGene(i)[1]);
        ind.setGene(changePosition, auxGene[0], ind.getGene(changePosition)[1]);
    }

    private void mutatePairVectorAngle(int i, I ind) {
        
        int newAngle = -1;
        while(newAngle == -1 || newAngle == ind.getGene(i)[1]){
            newAngle = GeneticAlgorithm.random.nextInt(4);
        }
        
        ind.setGene(i, ind.getGene(i)[0], newAngle);
        
//        int changePosition = -1;
//        while(changePosition == -1 || changePosition == i){
//            changePosition = GeneticAlgorithm.random.nextInt(15);
//        }
//        int[] auxGene = ind.getGene(i);
//        ind.setGene(i, ind.getGene(i)[0], ind.getGene(changePosition)[1]);
//        ind.setGene(changePosition, ind.getGene(changePosition)[1], auxGene[1]);
    }
}