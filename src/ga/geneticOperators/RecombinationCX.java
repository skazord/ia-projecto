/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga.geneticOperators;

import ga.GeneticAlgorithm;
import ga.Individual;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Rodrigues
 */
public class RecombinationCX <I extends Individual> extends Recombination<I>{
    public RecombinationCX(double probability) {
        super(probability);
    }

    @Override
    public void run(I ind1, I ind2) {
        int n = ind1.getNumGenes();

        if (n != ind2.getNumGenes()) {
            try {
                throw new Exception("permutations not same size");
            } catch (Exception ex) {
                Logger.getLogger(RecombinationPMX.class.getName()).log(Level.SEVERE, null, ex);
            }
            return;
        }

        HashMap<Integer,ArrayList<Integer>> cycles = new HashMap<Integer, ArrayList<Integer>>();
        ArrayList<Integer> positionsUsed = new ArrayList<Integer>();
        int currentCycle = 0;
        // exchange between the cutting points
        for (int i = 0; i < n; i++) {
            if (!positionsUsed.contains(i)) {
                currentCycle++;
                if (currentCycle%2 != 0) {
                    lookCycle(ind1, ind2, currentCycle, cycles, i);
                }
                else{
                    lookCycle(ind2, ind1, currentCycle, cycles, i);
                }
                
                positionsUsed.addAll(cycles.get(currentCycle));
            }
        }
        
        for (int i = 1; i <= currentCycle; i+=2) {
            for (Integer pos : cycles.get(i)) {
                ind1.swapGenes(ind2, pos);
            }
        }
//        
//        sb1 = new StringBuilder();
//        sb2 = new StringBuilder();
//        
//        for (int i = 0; i < n; i++) {
//            sb1.append(ind1.getGene(i)[0] + ", ");
//            sb2.append(ind2.getGene(i)[0] + ", ");
//        }
//        System.out.println("Individuos: \n" + sb1 + "\n" + sb2);
//        System.out.println("END");
    }
    
    private void lookCycle(I first, I second, int currentCycle, HashMap<Integer,ArrayList<Integer>> cycles, int initialPos){
        ArrayList<Integer> array = new ArrayList<Integer>();
        cycles.put(currentCycle, array);
        int position = initialPos;
        array.add(position);
        position = first.getPosGene(second.getGene(position)[0]);
        while(position != initialPos){
            array.add(position);
            position = first.getPosGene(second.getGene(position)[0]);
        }
    }
    
    @Override
    public String toString(){
        return "CX recombination (" + probability + ")";
    }
    
}
