/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga.geneticOperators;

import cutOptimization.CutOptimizationIndividual;
import ga.GeneticAlgorithm;
import ga.Individual;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Gabriel Rodrigues
 */
public class RecombinationPMX <I extends Individual> extends Recombination<I> {
    public RecombinationPMX(double probability) {
        super(probability);
    }
    //RecombinationOneCut

    @Override
    public void run(I ind1, I ind2) {
        int n = ind1.getNumGenes();

        if (n != ind2.getNumGenes()) {
            try {
                throw new Exception("permutations not same size");
            } catch (Exception ex) {
                Logger.getLogger(RecombinationPMX.class.getName()).log(Level.SEVERE, null, ex);
            }
            return;
        }

        // select cutting points
        int cuttingPoint1 = GeneticAlgorithm.random.nextInt(n);
        int cuttingPoint2 = GeneticAlgorithm.random.nextInt(n - 1);

        if (cuttingPoint1 == cuttingPoint2) {
                cuttingPoint2 = n - 1;
        } else if (cuttingPoint1 > cuttingPoint2) {
                int swap = cuttingPoint1;
                cuttingPoint1 = cuttingPoint2;
                cuttingPoint2 = swap;
        }

        // exchange between the cutting points, setting up replacement arrays
        int[] replaceInd1 = new int[ind1.getNumGenes()];
        int[] replaceInd2 = new int[ind2.getNumGenes()];
        for (int i = cuttingPoint1; i <= cuttingPoint2; i++) {
            replaceInd1[i] = ind1.getGene(i)[0];
            replaceInd2[i] = ind2.getGene(i)[0];
            ind1.swapGenes(ind2, i);
        }

        // fill in remaining slots with replacements
        for (int i = 0; i < replaceInd1.length; i++) {
            if (replaceInd1[i] != 0) {
                ind1.swapGenesPositions(ind2, ind1.getPosGene(replaceInd2[i]), ind2.getPosGene(replaceInd1[i]));
            }
        }
//        for (int i = 0; i < n; i++) {
//            if ((i < cuttingPoint1) || (i > cuttingPoint2)) {
//
//                int[] geneParent1 = ind1.getGene(i);
//
//                if (!ind2.hasGene(geneParent1[0])) {
//                    for (int j = 0; j < n; j++) {
//                        if ((i < cuttingPoint1) || (i > cuttingPoint2)) {
//                            int[] geneParent2 = ind2.getGene(j);
//                            if (!ind1.hasGene(geneParent2[0])) {
//                                ind1.swapGenesPositions(ind2, i, j);
//                            }
//                        }
//                    }
//                }
//            }
//        }
    }
    
    @Override
    public String toString(){
        return "PMX recombination (" + probability + ")";
    }   
}
