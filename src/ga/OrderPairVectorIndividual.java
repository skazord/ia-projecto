/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga;

import ga.Individual;
import ga.Problem;
import java.util.ArrayList;

/**
 *
 * @author Gabriel Rodrigues
 */
public abstract class OrderPairVectorIndividual <P extends cutOptimization.CutOptimization> extends Individual<P, OrderPairVectorIndividual>{

    public static final int PAIR_ANGLE = 2;

    //protected int[] genome; //primeiro a peça e depois o seu angulo. [p1,45,p2,90,p3,0,p4,180, ...]
    public int[][] genome;   //[p1,p2,p3,p4,...]
                                //[a1,a2,a3,a4,...]
    
    public OrderPairVectorIndividual(P problem, int size, double prob1s) {
        super(problem);
        genome = new int[size][PAIR_ANGLE];
        ArrayList<Integer> ids = new ArrayList<Integer>();
        int aux = -1;
        
        for (int g = 0; g < genome.length; g++) {
            while(ids.contains(aux) || aux == -1){
                aux = problem.getPiece(GeneticAlgorithm.random.nextInt(problem.getNumItems())).getId();
            }
            ids.add(aux);
            genome[g][0] = aux;
            genome[g][1] = GeneticAlgorithm.random.nextInt(3);
            //chamar função para inicializar o indivíduo. (Parece que é aqui a criação da primeira população)
        }
    }

    public OrderPairVectorIndividual(OrderPairVectorIndividual<P> original) {
        super(original);
        this.genome = new int[original.genome.length][original.genome[0].length];
        for (int i = 0; i < original.genome.length; i++) { //matrix.length altura
            for (int j = 0; j < original.genome[0].length; j++) { //matrix[0].length largura
                genome[i][j] = original.genome[i][j];
            }
        }
        //System.arraycopy(original.genome, 0, genome, 0, genome.length);
    }
    
    public int getNumGenes() {
        return genome.length;
    }
    
    @Override
    public int[] getGene(int g) {                        
        return new int[]{genome[g][0], genome[g][1]}; //devolve o par-gen
    }

    @Override
    public boolean hasGene(int id) {
        for (int i = 0; i < genome.length; i++) {
            if (genome[i][0] == id) {
                return true;
            }
        }
        return false;
    }    
    
    public void setGene(int g, int piece, int angle) {
        genome[g][1] = angle;
        genome[g][0] = piece;
    }

    public void swapGenes(OrderPairVectorIndividual other, int g) {
        int[] aux = genome[g];
        genome[g] = other.genome[g];
        other.genome[g] = aux;
        //definir o método de combonação mais apropriado.
    }

    public int[][] getGenome() {
        return genome;
    }    

    @Override
    public int getPosGene(int idGene) {
        for (int i = 0; i < genome.length; i++) {
            if (genome[i][0] == idGene) {
                return i;
            }
        }
        
        return -1;
    }

    @Override
    public void swapGenesPositions(OrderPairVectorIndividual other, int thisPosition, int otherPosition) {
        int[] aux = genome[thisPosition];
        genome[thisPosition] = other.genome[otherPosition];
        other.genome[otherPosition] = aux;
    }
}
