package ga;

public abstract class Individual<P extends Problem, I extends Individual>{

    protected double fitness;
    protected P problem;

    public Individual(P problem) {
        this.problem = problem;
    }

    public Individual(Individual<P, I> original) {
        this.problem = original.problem;
        this.fitness = original.fitness;
    }

    public abstract double computeFitness();
    
    public abstract int getNumGenes();
    
    public abstract void swapGenes(I other, int g);
    
    public abstract void swapGenesPositions(I other, int thisPosition, int otherPosition);
    
    public abstract boolean hasGene(int id);
    
    public abstract int[] getGene(int position);
    
    public abstract int getPosGene(int idGene);
    
    public abstract void setGene(int pos, int piece, int angle);

    public double getFitness() {
        return fitness;
    }

    @Override
    public abstract Object clone();
}
