package ga.selectionMethods;

import ga.GeneticAlgorithm;
import ga.Individual;
import ga.Population;
import ga.Problem;
import java.util.Random;
import javafx.scene.input.KeyCode;

public class Tournament <I extends Individual, P extends Problem<I>> extends SelectionMethod<I, P> {

    private int size;

    public Tournament(int popSize) {
        this(popSize, 2);
    }

    public Tournament(int popSize, int size) {
        super(popSize);
        this.size = size;
    }

    public Population<I, P> run(Population<I, P> original) {
        Population<I, P> result = new Population<I, P>(original.getSize());        

        for (int i = 0; i < popSize; i++) {
            result.addIndividual(tournament(original));
        }
        return result;
    }

    private I tournament(Population<I, P> population) {
        //TODO
//        I melhorIndividuo, outroIndividuo;
//        
//        for(int i = 0; i < size; i++){
//            outroIndividuo = population.getIndividual(GeneticAlgorithm.random.nextInt(popSize));
//            melhorIndividuo = population.getIndividual(GeneticAlgorithm.random.nextInt(popSize));
//            melhorIndividuo = (I) ((melhorIndividuo.getFitness() > outroIndividuo.getFitness())? melhorIndividuo : outroIndividuo.clone());
//        }
//        
//        return melhorIndividuo;
        
        int aux, best = GeneticAlgorithm.random.nextInt(popSize);
        
        for (int i = 0; i < size; i++) {
            aux = GeneticAlgorithm.random.nextInt(popSize);
            if (population.getIndividual(aux).getFitness() < population.getIndividual(best).getFitness()) {
                best = aux;
            }
        }
        
        return (I) population.getIndividual(best).clone();
    }
    
    @Override
    public String toString(){
        return "Tournament(" + size + ")";
    }    
}