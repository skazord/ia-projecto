package knapsack;

import ga.BitVectorIndividual;

public class KnapsackIndividual extends BitVectorIndividual <Knapsack>{

    private double value;
    private double weight;
    
    public KnapsackIndividual(Knapsack problem, int size, double prob1s){
        super(problem, size, prob1s);
    }
    
    public KnapsackIndividual(KnapsackIndividual original) {
        super(original);
        this.weight = original.weight;
        this.value = original.value;
    }
    
    public double computeFitness() {
        
        value = 0;
        weight = 0;
        for (int i = 0; i < genome.length; i++) {
            if (genome[i] == ONE) {
                value += problem.getItem(i).getValue();
                weight += problem.getItem(i).getWeight();
            } 
        }
        
        fitness = value;
        
        switch(problem.getFitnessType()){
            case Knapsack.SIMPLE_FITNESS:
                fitness = (weight > problem.getMaximumWeight())? 0 : value;
                break;
            case Knapsack.PENALTY_FITNESS:
                fitness = (weight > problem.getMaximumWeight())? getPenalty(value, fitness) : value;
                break;
        }
        return fitness;
    }    
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("\nWeight: " + weight + " (limit: " + problem.getMaximumWeight() + ")");
        sb.append("\nValue: " + value);        
        sb.append("\nfitness: " + fitness);        
        sb.append("\nItems: ");
        for (int i = 0; i < genome.length; i++) {
            if (genome[i] == ONE) {
                sb.append(problem.getItem(i));
            }
        }
        return sb.toString();
    }
    
    @Override
    public Object clone() {
        return new KnapsackIndividual(this);
    }    

    private double getPenalty(double val, double fit) {
        double penalty = Math.exp(problem.getMaximumWeight() - weight)*value;
        System.out.println("math " + Math.exp(problem.getMaximumWeight() - weight));
        System.out.println("value " + Math.exp(problem.getMaximumWeight() - weight)*value);
        fit = penalty;
        return fit;
    }

    @Override
    public void swapGenesPositions(BitVectorIndividual other, int thisPosition, int otherPosition) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean hasGene(int id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int[] getGene(int position) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getPosGene(int idGene) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setGene(int pos, int piece, int angle) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
