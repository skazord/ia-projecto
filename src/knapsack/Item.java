package knapsack;

public class Item {

    public String name;
    public double weight;
    public double value;

    public Item(String name, double weight, double value) {
        this.name = name;
        this.weight = weight;
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public double getValue() {
        return value;
    }
    
    

    @Override
    public String toString() {
        return "\n" + name + "\t" + weight + "\t" + value;
    }
}
