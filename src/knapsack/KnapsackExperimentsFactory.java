package knapsack;

import cutOptimization.CutOptimization;
import cutOptimization.CutOptimizationIndividual;
import experiments.*;
import ga.GAListener;
import ga.GeneticAlgorithm;
import ga.geneticOperators.*;
import ga.selectionMethods.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import statistics.StatisticBestAverage;
import statistics.StatisticBestInRun;

public class KnapsackExperimentsFactory extends ExperimentsFactory {

    private int populationSize;
    private int maxGenerations;
    private SelectionMethod<CutOptimizationIndividual, CutOptimization> selection;
    private Recombination<CutOptimizationIndividual> recombination;
    private Mutation<CutOptimizationIndividual> mutation;
    private CutOptimization cutOptimization;
    private Experiment<ExperimentsFactory, Knapsack> experiment;

    public KnapsackExperimentsFactory(File configFile) throws IOException {
        super(configFile);
    }

    public Experiment buildExperiment() throws IOException {
        numRuns = Integer.parseInt(getParameterValue("Runs"));
        populationSize = Integer.parseInt(getParameterValue("Population size"));
        maxGenerations = Integer.parseInt(getParameterValue("Max generations"));

        //SELECTION
        if (getParameterValue("Selection").equals("tournament")) {
            int tournamentSize = Integer.parseInt(getParameterValue("Tournament size"));
            selection = new Tournament<CutOptimizationIndividual, CutOptimization>(populationSize, tournamentSize);
        } else if (getParameterValue("Selection").equals("roulette wheel")) {
            selection = new RouletteWheel<CutOptimizationIndividual, CutOptimization>(populationSize);
        }

        //RECOMBINATION
        double recombinationProbability = Double.parseDouble(getParameterValue("Recombination probability"));
        if (getParameterValue("Recombination").equals("PMX")) {
            recombination = new RecombinationPMX<CutOptimizationIndividual>(recombinationProbability);
        } else if (getParameterValue("Recombination").equals("OX1")) {
            recombination = new RecombinationOX1<CutOptimizationIndividual>(recombinationProbability);
        } else if (getParameterValue("Recombination").equals("CX")) {
            recombination = new RecombinationCX<CutOptimizationIndividual>(recombinationProbability);
        }

        //MUTATION
        double mutationProbability = Double.parseDouble(getParameterValue("Mutation probability"));
        if (getParameterValue("Mutation").equals("binary")) {
            mutation = new MutationOrderPair<CutOptimizationIndividual>(mutationProbability);
        }

        //PPROBABILILTY OF 1S AND FITNESS TYPE
        int fitnessType = Integer.parseInt(getParameterValue("Fitness type"));

        //PROBLEM
        cutOptimization = CutOptimization.buildKnapsack(getParameterValue("Problem file"));
        cutOptimization.setFitnessType(fitnessType);

        String textualRepresentation = buildTextualExperiment();

        experiment = new Experiment(this, numRuns, cutOptimization, textualRepresentation);

        statistics = new ArrayList<ExperimentListener>();
        for (String statisticName : statisticsNames) {
            ExperimentListener statistic = buildStatistic(statisticName);
            statistics.add(statistic);
            experiment.addExperimentListener(statistic);
        }

        return experiment;
    }

    public GeneticAlgorithm generateGAInstance(int seed) {
        GeneticAlgorithm<CutOptimizationIndividual, CutOptimization> ga;

        ga = new GeneticAlgorithm<CutOptimizationIndividual, CutOptimization>(
                populationSize,
                maxGenerations,
                selection,
                recombination,
                mutation,
                new Random(seed));

        for (ExperimentListener statistic : statistics) {
            ga.addGAListener((GAListener) statistic);
        }

        return ga;
    }

    private ExperimentListener buildStatistic(String statisticName) {
        if (statisticName.equals("BestIndividual")) {
            return new StatisticBestInRun();
        }
        if (statisticName.equals("BestAverage")) {
            return new StatisticBestAverage(numRuns);
        }        
        return null;
    }

    private String buildTextualExperiment() {
        StringBuilder sb = new StringBuilder();
        sb.append("Population size:" + populationSize + "\t");
        sb.append("Max generations:" + maxGenerations + "\t");
        sb.append("Selection:" + selection + "\t");
        sb.append("Recombination:" + recombination + "\t");
        sb.append("Mutation:" + mutation + "\t");
        sb.append("Fitness type:" + cutOptimization.getFitnessType());
        return sb.toString();
    }
}
